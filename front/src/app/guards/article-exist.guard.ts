import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FeedService } from '../services/feed.service';

@Injectable({
  providedIn: 'root'
})
export class ArticleExistGuard implements CanActivate {

  constructor(
    private feedService: FeedService,
    private router: Router
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    const articleId = next.params['id'];

    return this.feedService.feed$.pipe(
      map(articles => {
        const articleExists = articles.some(article => article.id.toString() === articleId);
        if (!articleExists) {
          this.router.navigate(['/page-not-found']);
        }
        return articleExists;
      })
    );
  }
}
