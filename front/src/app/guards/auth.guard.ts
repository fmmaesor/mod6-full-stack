import {Injectable} from "@angular/core";
import {CanActivate, Router} from "@angular/router";
import { SessionService } from "../services/session.service";
import { Observable } from "rxjs";

@Injectable({providedIn: 'root'})
export class AuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private sessionService: SessionService,
  ) {
  }

  public canActivate(): boolean {
    this.sessionService.$isLogged().subscribe((isLogged: boolean) => {
      if (isLogged === false) {
        this.router.navigate([''])
        return false;
      }
      else {return true};

    });
    return true
  }
}
