import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Article } from '../interfaces/article.interface';
import { ArticleRequest } from '../interfaces/articleRequest';

@Injectable({
  providedIn: 'root',
})
export class ArticleService {
  private pathService = 'api/articles';

  constructor(private httpClient: HttpClient) {}

  public getFeed(userMail: String): Observable<Array<Article>> {
    return this.httpClient.get<Array<Article>>(`${this.pathService}/feed/${userMail}` );
  }

  public create(userMail: String, article: ArticleRequest): Observable<Article>{
    return this.httpClient.post<Article>(`${this.pathService}/create/${userMail}`, article);
  }
}
