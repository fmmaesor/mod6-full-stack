import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Article } from 'src/app/interfaces/article.interface';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { ArticleService } from './article.service';

@Injectable({
  providedIn: 'root',
})
export class FeedService {
  private feedSubject = new BehaviorSubject<Article[]>([]);
  private storedFeed: Article[] = [];

  get feed$() {
    return this.feedSubject.asObservable();
  }

  constructor(private router: Router, private articleService: ArticleService) {
    this.router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe(() => {
        this.feedSubject.next(this.storedFeed);
      });
  }

  public setFeed(articles: Article[]) {
    this.storedFeed = articles;
  }

  public addArticle(article: Article) {
    this.storedFeed.push(article);
  }

  public updateFeedOnSubscriptionChange(userEmail: string): void {
    this.articleService.getFeed(userEmail).subscribe(
      (updatedFeed) => {
        const currentFeed = this.feedSubject.getValue();
        if (!this.isEqualFeed(currentFeed, updatedFeed)) {
          this.feedSubject.next(updatedFeed);
        }
      },
      (error) => {
        console.error('Error fetching feed for subjects:', error);
      }
    );
  }

  public isEqualFeed(feed1: Article[], feed2: Article[]): boolean {
    if (feed1.length !== feed2.length) {
      return false;
    }
    for (let i = 0; i < feed1.length; i++) {
      if (feed1[i].id !== feed2[i].id) {
        return false;
      }
    }
    return true;
  }
}
