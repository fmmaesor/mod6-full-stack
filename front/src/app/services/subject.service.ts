import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Subject } from '../interfaces/subject.interface';

@Injectable({
  providedIn: 'root'
})
export class SubjectService {
  private pathService = 'api/subjects';

  private subjectsSubject = new BehaviorSubject<Subject[]>([]);
  private storedSubjects: Subject[] = [];
  private subjectsFetched = false;


  get subjects$() {
    return this.subjectsSubject.asObservable();
  }

  private userSubjectsSubject = new BehaviorSubject<Subject[]>([]);


  get userSubjects$() {
    return this.userSubjectsSubject.asObservable();
  }

  constructor(private httpClient: HttpClient) { }

  public getUserSubjects(userMail: String): Observable<Array<Subject>> {
    return this.httpClient.get<Array<Subject>>(`${this.pathService}/user/${userMail}`);
  }

  fetchSubjectsIfNeeded(): void {
    if (!this.subjectsFetched) {
      this.httpClient.get<Subject[]>(`${this.pathService}`).subscribe(
        subjects => {
          this.subjectsSubject.next(subjects);
          this.subjectsFetched = true;
        },
        error => {
          console.error('Error fetching subjects:', error);
        }
      );
    }
  }
}
