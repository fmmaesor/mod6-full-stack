import { CommentRequest } from './../interfaces/commentRequest';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Comment } from '../interfaces/comment.interface';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  private pathService = 'api/comments';

  constructor(private httpClient: HttpClient) {}

  public getByArticleId(articleId: number): Observable<Array<Comment>> {
    return this.httpClient.get<Array<Comment>>(`${this.pathService}/`+ articleId.toString());
  }

  public create(userMail: String,  comment: CommentRequest):  Observable<Comment> {
    return this.httpClient.post<Comment>(`${this.pathService}/create/${userMail}`, comment);
  }

}
