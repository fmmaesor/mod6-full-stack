import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { User } from '../interfaces/user.interface';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  public user: User | undefined;

  private isLoggedSubject = new BehaviorSubject<boolean>(this.isLoggedIn());

  public $isLogged(): Observable<boolean> {
    return this.isLoggedSubject.asObservable();
  }

  public logIn(user: User): void {
    this.user = user;
    localStorage.setItem('isLogged', 'true');
    const loggedUser = JSON.stringify(this.user);
    localStorage.setItem('user', loggedUser)
    this.isLoggedSubject.next(true);
  }

  public logOut(): void {
    localStorage.clear()
    this.user = undefined;
    this.isLoggedSubject.next(false);
  }

  private isLoggedIn(): boolean {
    return localStorage.getItem('isLogged') === 'true';
  }
}
