import { UserRequest } from './../interfaces/userRequest.interface';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../interfaces/user.interface';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private pathService = 'api/users';

  private usersSubject = new BehaviorSubject<User[]>([]);
  public users$ = this.usersSubject.asObservable();
  private usersFetched = false;

  constructor(private httpClient: HttpClient) { }

  public getUserById(id: number): Observable<User> {
    return this.httpClient.get<User>(`${this.pathService}/${id}`);
  }

  public fetchUsersIfNeeded(): void {
    if (!this.usersFetched) {
      this.httpClient.get<User[]>(`${this.pathService}`).subscribe(
        users => {
          this.usersSubject.next(users);
          this.usersFetched = true;
        },
        error => {
          console.error('Error fetching users:', error);
        }
      );
    }
  }

  updateUser(userMail: String, updatedUserData: UserRequest): Observable<User> {
    return this.httpClient.put<User>(`${this.pathService}/update/${userMail}`, updatedUserData);
  }

  public subscribeSubject(userMail: String, subjectId: number): Observable<User>  {
    return this.httpClient.put<User>(`${this.pathService}/subscribe/${userMail}`, subjectId);
  }

  public unsubscribeSubject(userMail: String, subjectId: number): Observable<User>  {
    return this.httpClient.put<User>(`${this.pathService}/unsubscribe/${userMail}`, subjectId);
  }
}
