import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session.service';
import { AuthService } from 'src/app/services/auth.service';
import { RegisterRequest } from 'src/app/interfaces/registerRequest.interface';
import { AuthSuccess } from 'src/app/interfaces/authSuccess.interface';
import { User } from 'src/app/interfaces/user.interface';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PasswordValidationService } from 'src/app/services/password-validation.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {

  public onError = false;

  public form = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    name: ['', [Validators.required, Validators.minLength(4)]],
    password: ['', [Validators.required, this.passwordValidationService.passwordValidator()]]
  });

  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private router: Router,
    private snackbar: MatSnackBar,
    private passwordValidationService: PasswordValidationService,) { }

  public submit(): void {
    const registerRequest = this.form.value as RegisterRequest;
    this.authService.register(registerRequest).subscribe(
      (response: AuthSuccess) => {
        localStorage.setItem('token', response.token);
        this.snackbar.open('Utilisateur crée', 'Close', {
          duration: 500,
          panelClass: ['snackbar']
        });
        this.router.navigate(['/login'])
      },
      (error) =>  {
        this.snackbar.open('un erreur s\'est produit ', 'Close', {
        duration: 500,
        panelClass: ['error-snackbar']
      });
        this.onError = true
    }
    );
  }

}
