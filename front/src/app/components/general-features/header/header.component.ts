import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { SessionService } from 'src/app/services/session.service';
import { MatMenuTrigger } from '@angular/material/menu';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(
    private sessionService: SessionService,
    private router: Router) { }

  ngOnInit(): void {
  }

  isFeedPage(): boolean {
    return this.router.url === '/feed';
  }

  isUserPage(): boolean {
    return this.router.url === '/user-profile';
  }

  back(): void {
    window.history.back();
  }

  public $isLogged(): Observable<boolean> {
    return this.sessionService.$isLogged();
  }

}
