import { UserService } from './../../../services/user.service';
import { Subject } from 'src/app/interfaces/subject.interface';
import { SubjectService } from './../../../services/subject.service';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FeedService } from 'src/app/services/feed.service';
import { User } from 'src/app/interfaces/user.interface';

@Component({
  selector: 'app-subjects',
  templateUrl: './subjects.component.html',
  styleUrls: ['./subjects.component.scss']
})
export class SubjectsComponent implements OnInit {


  public subjects: Subject[] = [];

  public userSubjects: Subject[] = [];

  public currentUser: User = JSON.parse(localStorage.getItem('user')!);

  constructor(
    private subjectService: SubjectService,
    private userService: UserService,
    private feedService: FeedService,
    private snackbar: MatSnackBar) { }

  ngOnInit(): void {
    this.subjectService.subjects$.subscribe(subjects => {
      this.subjects = subjects;
    });
    this.subjectService.fetchSubjectsIfNeeded();


   this.subjectService.getUserSubjects(this.currentUser.email).subscribe(subjects => {
      this.userSubjects = subjects;});

  }

  public isSubscribed(subjectId: number): boolean {
    return this.userSubjects.some(userSubject => userSubject.id === subjectId);
  }

  subscribe(subjectId: number): void {
    this.userService.subscribeSubject(this.currentUser.email, subjectId).subscribe(() => {
      this.feedService.updateFeedOnSubscriptionChange(this.currentUser.email);
      this.snackbar.open('Vous étés maintenant abonné', 'Close', {
        duration: 500,
        panelClass: ['snackbar']
      });
      var subjectSubscribed = this.subjects.find(subscribedSubject => subscribedSubject.id === subjectId)
      if (subjectSubscribed !== undefined) {
        this.userSubjects.push(subjectSubscribed);
      }
    }, error => {
      this.snackbar.open('un erreur s\'est produit ', 'Close', {
        duration: 500,
        panelClass: ['error-snackbar']
      });
      console.error('Error subscribing:', error);
    });;
    }

}
