import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Comment } from 'src/app/interfaces/comment.interface';
import { CommentRequest } from 'src/app/interfaces/commentRequest';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CommentService } from 'src/app/services/comment.service';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/interfaces/user.interface';


@Component({
  selector: 'app-comment-creation',
  templateUrl: './comment-creation.component.html',
  styleUrls: ['./comment-creation.component.scss']
})
export class CommentCreationComponent implements OnInit {

  @Output() commentAdded = new EventEmitter<Comment>();

  public form = this.fb.group({
    description: ['', [Validators.required]]
  });

  public currentUser: User = JSON.parse(localStorage.getItem('user')!);

  constructor(private commentService: CommentService,
    private fb: FormBuilder,
    private snackbar: MatSnackBar,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
  }


  public submit(): void {
    const commentRequest = this.form.value as CommentRequest;
    commentRequest.articleId = this.route.snapshot.params['id'];
    this.commentService
      .create(this.currentUser.email, commentRequest)
      .subscribe((response: Comment) => {
        this.snackbar.open('Commentaire ajouté', 'Close', {
          duration: 2000,
          panelClass: ['snackbar'],
        });
        this.commentAdded.emit(response);
      });
    this.form.reset();
    this.snackbar.open("Un erreur s'est produit", 'Close', {
      duration: 2000,
      panelClass: ['error-snackbar'],
    });
  }
}


