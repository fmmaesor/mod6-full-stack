import { ArticleService } from './../../../services/article.service';
import { Component, OnInit } from '@angular/core';
import { Article } from 'src/app/interfaces/article.interface';
import { Subject } from 'src/app/interfaces/subject.interface';
import { User } from 'src/app/interfaces/user.interface';
import { FeedService } from 'src/app/services/feed.service';
import { SubjectService } from 'src/app/services/subject.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss'],
})
export class FeedComponent implements OnInit {

  public feed: Article[] = [];

  public subjects: Subject[] = [];

  sortOrder: 'asc' | 'desc' = 'asc';

  public currentUser: User = JSON.parse(localStorage.getItem('user')!);

  constructor(
    private articleService: ArticleService,
    private subjectService: SubjectService,
    private userService: UserService,
    private  feedService: FeedService) {}

  ngOnInit(): void {
    this.feedService.feed$.subscribe(
      articles => {
        if (articles.length === 0 || !this.feedService.isEqualFeed(this.feed, articles)) {
          this.articleService.getFeed(this.currentUser.email).subscribe(
            newArticles => {
              if (!this.feedService.isEqualFeed(this.feed, newArticles)) {
                this.feed = newArticles;
                this.feedService.setFeed(newArticles);
              }
            },
            error => {
              console.error('Error fetching feed:', error);
            }
          );
        }
      }
    );

    this.subjectService.fetchSubjectsIfNeeded();
    this.userService.fetchUsersIfNeeded();
  }

  get sortedFeed(): Article[] {
    return this.feed.slice().sort((a, b) => {
      const dateA = new Date(a.createdAt);
      const dateB = new Date(b.createdAt);
      if (this.sortOrder === 'asc') {
        return dateA.getTime() - dateB.getTime();
      } else {
        return dateB.getTime() - dateA.getTime();
      }
    });
  }

  public getUserName(userId: number): string {
    let userName = ''
    this.userService.users$.subscribe((users) => {
      const user = users.find((user) => user.id === userId);
      userName = user ? user.name : 'Unknown User';
    })
    return userName
  }
}
