import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Article } from 'src/app/interfaces/article.interface';
import { ArticleRequest } from 'src/app/interfaces/articleRequest';
import { Subject } from 'src/app/interfaces/subject.interface';
import { User } from 'src/app/interfaces/user.interface';
import { ArticleService } from 'src/app/services/article.service';
import { SubjectService } from 'src/app/services/subject.service';

@Component({
  selector: 'app-article-creation',
  templateUrl: './article-creation.component.html',
  styleUrls: ['./article-creation.component.scss'],
})
export class ArticleCreationComponent implements OnInit {
  public onError = false;

  public form = this.fb.group({
    title: ['', [Validators.required]],
    description: ['', [Validators.required]],
    subjectTitle: ['', Validators.required],
  });

  public subjects: Subject[] = []

  public currentUser: User = JSON.parse(localStorage.getItem('user')!);

  constructor(
    private articleService: ArticleService,
    private subjectService: SubjectService,
    private fb: FormBuilder,
    private router: Router,
    private snackbar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.subjectService.subjects$.subscribe(subjects => {
      this.subjects = subjects;
    });
    this.subjectService.fetchSubjectsIfNeeded();

  }

  public submit(): void {
    const articleRequest = this.form.value as ArticleRequest;
    this.articleService
      .create(this.currentUser.email, articleRequest)
      .subscribe((response: Article) => {
        this.snackbar.open('Article bien crée', 'Close', {
          duration: 2000,
          panelClass: ['snackbar'],
        });
        this.router.navigate(['/feed']);
      });
    this.form.reset()
    this.snackbar.open("Un erreur s'est produit", 'Close', {
      duration: 2000,
      panelClass: ['error-snackbar'],
    });
  }
}
