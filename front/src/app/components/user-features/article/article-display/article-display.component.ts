import { SubjectService } from 'src/app/services/subject.service';
import { CommentService } from '../../../../services/comment.service';
import { Article } from '../../../../interfaces/article.interface';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Comment } from 'src/app/interfaces/comment.interface';
import { FeedService } from 'src/app/services/feed.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-article-display',
  templateUrl: './article-display.component.html',
  styleUrls: ['./article-display.component.scss'],
})
export class ArticleDisplayComponent implements OnInit {
  @Input() article: Article | undefined;

  public comments: Comment[] = [];

  public subjectName: string = "";
  public userName: string = "";

  constructor(
    private route: ActivatedRoute,
    private commentService: CommentService,
    private feedService: FeedService,
    private subjectService: SubjectService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    const articleId = this.route.snapshot.params['id'];

    this.feedService.feed$.subscribe((articles) => {
      this.article = articles.find(
        (article) => article.id.toString() === articleId
      );

      if (this.article) {
        this.subjectService.subjects$.subscribe((subjects) => {
          const subject = subjects.find(
            (subject) => subject.id === this.article?.subjectId
          );
          if (subject) {
             this.subjectName = subject.title;
          }

        this.userService.users$.subscribe((users) => {
          const user = users.find(
            (user) => user.id === this.article?.userId
          );
          if (user) {
            this.userName = user.name;
          }
        })
        });

        this.commentService.getByArticleId(articleId).subscribe((comments) => {
          this.comments = comments;
        });
      }
    });
  }

  public addComment(comment: Comment) {
    this.comments.push(comment);
  }
}
