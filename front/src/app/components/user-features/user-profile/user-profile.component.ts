import { SessionService } from './../../../services/session.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup} from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/interfaces/user.interface';
import { Subject } from 'src/app/interfaces/subject.interface';
import { SubjectService } from 'src/app/services/subject.service';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FeedService } from 'src/app/services/feed.service';
import { PasswordValidationService } from 'src/app/services/password-validation.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss'],
})
export class UserProfileComponent implements OnInit {
  public hide = true;

  userForm!: FormGroup;
  user = { name: '', email: '' };
  public subjects: Subject[] = [];

  public currentUser: User = JSON.parse(localStorage.getItem('user')!);

  constructor(
    private authService: AuthService,
    private sessionService: SessionService,
    private subjectService: SubjectService,
    private userService: UserService,
    private feedService: FeedService,
    private passwordValidationService: PasswordValidationService,
    private router: Router,
    private fb: FormBuilder,
    private snackbar: MatSnackBar
  ) {}

  public ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('user')!);

    this.subjectService.getUserSubjects(this.user.email).subscribe((subjects) => {
      this.subjects = subjects;
    });

    this.userForm = this.fb.group({
      name: [this.user.name],
      password: [''],
    });

    this.userForm.get('password')?.valueChanges.subscribe((value) => {
      if (value) {
        this.userForm
          .get('password')
          ?.setValidators(this.passwordValidationService.passwordValidator());
      } else {
        this.userForm.get('password')?.clearValidators();
      }
    });
  }

  public unsubscribe(subjectId: number) {
    this.userService.unsubscribeSubject(this.currentUser.email, subjectId).subscribe(
      () => {
        this.subjects = this.subjects.filter(
          (subject) => subject.id !== subjectId
        );
        this.feedService.updateFeedOnSubscriptionChange(this.currentUser.email);
        this.snackbar.open('Vous étés plus abonné', 'Close', {
          duration: 500,
          panelClass: ['snackbar'],
        });
      },
      (error) => {
        this.snackbar.open("un erreur s'est produit ", 'Close', {
          duration: 500,
          panelClass: ['error-snackbar'],
        });
        console.error('Error subscribing:', error);
      }
    );
  }

  public save(): void {
    if (this.userForm.valid) {
      const userRquest = this.userForm.value;

      this.userService.updateUser(this.currentUser.email, userRquest).subscribe(
        (response: User) => {
          this.userForm.reset({
            name: '',
            password: '',
          });
          this.snackbar.open('Utilisateur bien modifié', 'Close', {
            duration: 500,
            panelClass: ['snackbar'],
          });
          this.user = response
          const loggedUser = JSON.stringify(this.user);
          localStorage.setItem('user', loggedUser);
        },
        (error) => {
          this.snackbar.open("Un erreur s'est produit ", 'Close', {
            duration: 500,
            panelClass: ['error-snackbar'],
          });
        }
      );
    }
  }

  public logout(): void {
    this.sessionService.logOut();
    this.router.navigate(['']);
  }
}
