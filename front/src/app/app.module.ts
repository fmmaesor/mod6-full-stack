
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import {MatSelectModule} from '@angular/material/select';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { JwtInterceptor } from './interceptors/jwt.interceptor';

import { RegisterComponent } from './components/auth/register/register.component';
import { LoginComponent } from './components/auth/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { UserProfileComponent } from './components/user-features/user-profile/user-profile.component';
import { HeaderComponent } from './components/general-features/header/header.component';
import { FeedComponent } from './components/user-features/feed/feed.component';
import { SubjectsComponent } from './components/user-features/subjects/subjects.component';
import { ArticleCreationComponent } from './components/user-features/article/article-creation/article-creation.component';
import { ArticleDisplayComponent } from './components/user-features/article/article-display/article-display.component';
import { CommentCreationComponent } from './components/user-features/comment-creation/comment-creation.component';

import { FeedService } from './services/feed.service';
import { PasswordValidationService } from './services/password-validation.service';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';

const materialModules = [
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatSnackBarModule,
  MatSelectModule
]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RegisterComponent,
    LoginComponent,
    UserProfileComponent,
    HeaderComponent,
    FeedComponent,
    SubjectsComponent,
    ArticleCreationComponent,
    ArticleDisplayComponent,
    CommentCreationComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    CommonModule,
    ReactiveFormsModule,
    ...materialModules
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    FeedService,
    PasswordValidationService
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
