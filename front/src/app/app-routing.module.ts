import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from 'src/app/components/auth/login/login.component';
import { RegisterComponent } from 'src/app/components/auth//register/register.component';
import { UserProfileComponent } from './components/user-features/user-profile/user-profile.component';
import { FeedComponent } from './components/user-features/feed/feed.component';
import { SubjectsComponent } from './components/user-features/subjects/subjects.component';
import { AuthGuard } from './guards/auth.guard';
import { ArticleCreationComponent } from './components/user-features/article/article-creation/article-creation.component';
import { ArticleDisplayComponent } from './components/user-features/article/article-display/article-display.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { ArticleExistGuard } from './guards/article-exist.guard';


const routes: Routes =
[
  { title: 'MDD', path: '', component: HomeComponent },
  { title: 'Se Connecter', path: 'login', component: LoginComponent },
  { title: 'Inscription', path: 'register', component: RegisterComponent },
  { title: 'Profil', path: 'user-profile', component: UserProfileComponent, canActivate: [AuthGuard] },
  { title: 'Thémes', path: 'subjects', component: SubjectsComponent, canActivate: [AuthGuard] },
  { title: 'Dernières articles', path: 'feed', component: FeedComponent, canActivate: [AuthGuard] },
  { title: 'Créer un article', path: 'creer-article', component: ArticleCreationComponent, canActivate: [AuthGuard] },
  { path: 'article/:id', component: ArticleDisplayComponent, canActivate: [ArticleExistGuard]  },
  { title: 'Page not Found', path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
