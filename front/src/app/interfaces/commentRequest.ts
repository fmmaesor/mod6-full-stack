export interface CommentRequest {
    description: string;
    articleId: number;
}
