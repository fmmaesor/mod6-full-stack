export interface Comment {
  description: string;
  userId: number;
  articleId: number;
}
