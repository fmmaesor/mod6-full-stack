export interface Article {
  id: number,
	title: string,
	description: string,
  createdAt: Date,
  userId: number,
  subjectId: number
}
