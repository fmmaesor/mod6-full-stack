package com.openclassrooms.mddapi.dto;

public class LoginRequest {

  private String identifier;
  private String password;


  public String getIdentifier() {
    return identifier;
  }

  public void setEmail(String identifier) {
    this.identifier = identifier;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

}
