package com.openclassrooms.mddapi.services;

import com.openclassrooms.mddapi.domain.Article;
import com.openclassrooms.mddapi.domain.Subject;
import com.openclassrooms.mddapi.domain.User;
import com.openclassrooms.mddapi.dto.ArticleDto;
import com.openclassrooms.mddapi.dto.ArticleRequest;
import com.openclassrooms.mddapi.dto.UserDto;
import com.openclassrooms.mddapi.mapper.ArticleMapper;
import com.openclassrooms.mddapi.mapper.SubjectMapper;
import com.openclassrooms.mddapi.mapper.UserMapper;
import com.openclassrooms.mddapi.repository.ArticleRepository;
import com.openclassrooms.mddapi.repository.UserRepository;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ArticleService {

    private final ArticleRepository articleRepository;
    private final ArticleMapper articleMapper;
    private final UserService userService;
    private final UserMapper userMapper;
    private final UserRepository userRepository;
    private final SubjectService subjectService;
    private final SubjectMapper subjectMapper;

    public ArticleService(ArticleRepository articleRepository,
                          ArticleMapper articleMapper,
                          UserService userService,
                          UserMapper userMapper,
                          UserRepository userRepository,
                          SubjectService subjectService,
                          SubjectMapper subjectMapper){
        this.articleRepository = articleRepository;
        this.articleMapper = articleMapper;
        this.userService = userService;
        this.userMapper = userMapper;
        this.userRepository = userRepository;
        this.subjectService = subjectService;
        this.subjectMapper = subjectMapper;
    }

    @Transactional(readOnly = true)
    public List<ArticleDto> getBySubjectId(Integer subjectId){
        List<Article> articles = articleRepository.findBySubjectId(subjectId);
        List<ArticleDto> articleDtos = articles.stream()
                .map(articleMapper::toDto)
                .collect(Collectors.toList());
        return articleDtos;
    }

    @Transactional(readOnly = true)
    public Article getById(Integer articleId){
        return articleRepository.findById(articleId).orElse(null);
    }

    public ArticleDto create(String userMail, ArticleRequest articleRequest){
        Article articleToSubmit = new Article();

        User currentUser = userRepository.findByEmail(userMail).orElse(null);

        Subject subject = this.subjectService.getByName(articleRequest.getSubjectTitle());

        articleToSubmit.setTitle(articleRequest.getTitle());
        articleToSubmit.setDescription(articleRequest.getDescription());
        articleToSubmit.setCreatedAt(LocalDateTime.now());
        articleToSubmit.setUser(currentUser);
        articleToSubmit.setSubject(subject);
        Article savedArticle = articleRepository.save(articleToSubmit);
        return articleMapper.toDto(savedArticle);
    }

    public List<ArticleDto> getFeed(String userMail){
        List<ArticleDto> feedArticlesList = new ArrayList<>();
        UserDto currentUser = this.userService.getByEmail(userMail).orElse(null);
        for (Integer subjectId: currentUser.getSubjectIds()
        ) {
            feedArticlesList.addAll(getBySubjectId(subjectId));
        }
        return  feedArticlesList;
    }


}
