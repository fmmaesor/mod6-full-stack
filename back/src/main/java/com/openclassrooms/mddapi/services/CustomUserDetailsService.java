package com.openclassrooms.mddapi.services;

import com.openclassrooms.mddapi.domain.Role;
import com.openclassrooms.mddapi.domain.User;
import com.openclassrooms.mddapi.repository.UserRepository;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component("userDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

  private final UserRepository userRepository;


  public CustomUserDetailsService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  public UserDetails loadUserByUsername(String identifier) throws UsernameNotFoundException {
    Optional<User> userByEmail = userRepository.findByEmail(identifier);
    if (userByEmail.isPresent()) {
      return userByEmail.get();
    }

    Optional<User> userByUsername = userRepository.findByName(identifier);
    if (userByUsername.isPresent()) {
      return userByUsername.get();
    }
    throw  new UsernameNotFoundException("User not found with username: " + identifier);
  }

  private List<GrantedAuthority> getGrantedAuthorities(String role) {
    List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
    authorities.add(new SimpleGrantedAuthority("ROLE_" + role));
    return authorities;
  }

  private String transformRoleIntoString(Role role) {
    return role.getName();
  }
}
