package com.openclassrooms.mddapi.services;

import com.openclassrooms.mddapi.domain.Article;
import com.openclassrooms.mddapi.domain.Comment;
import com.openclassrooms.mddapi.domain.User;
import com.openclassrooms.mddapi.dto.CommentDto;
import com.openclassrooms.mddapi.dto.CommentRequest;
import com.openclassrooms.mddapi.mapper.ArticleMapper;
import com.openclassrooms.mddapi.mapper.CommentMapper;
import com.openclassrooms.mddapi.mapper.UserMapper;
import com.openclassrooms.mddapi.repository.CommentRepository;
import com.openclassrooms.mddapi.repository.UserRepository;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class CommentService {

    private final CommentRepository commentRepository;
    private final CommentMapper commentMapper;
    private final UserService userService;
    private final UserMapper userMapper;
    private final UserRepository userRepository;
    private final ArticleService articleService;
    private final ArticleMapper articleMapper;

    public CommentService(CommentRepository commentRepository,
                          CommentMapper commentMapper,
                          UserService userService,
                          UserMapper userMapper,
                          UserRepository userRepository,
                          ArticleService articleService,
                          ArticleMapper articleMapper) {
        this.commentRepository = commentRepository;
        this.commentMapper = commentMapper;
        this.userService = userService;
        this.userMapper = userMapper;
        this.userRepository = userRepository;
        this.articleService = articleService;
        this.articleMapper = articleMapper;
    }

    @Transactional(readOnly = true)
    public List<CommentDto> getByArticleId(Integer articleId){
        List<Comment> comments = this.commentRepository.findByArticleId(articleId);
        return comments.stream()
                .map(commentMapper::toDto)
                .collect(Collectors.toList());
    }

    public CommentDto create(String userMail, CommentRequest commentRequest){
        Comment commentToSubmit = new Comment();

                User currentUser = userRepository.findByEmail(userMail).orElse(null);

        Article article = this.articleService.getById(commentRequest.getArticleId());

        commentToSubmit.setDescription(commentRequest.getDescription());
        commentToSubmit.setCreatedAt(LocalDateTime.now());
        commentToSubmit.setUser(currentUser);
        commentToSubmit.setArticle(article);
        Comment savedComment = commentRepository.save(commentToSubmit);
        return commentMapper.toDto(savedComment);
    }
}

