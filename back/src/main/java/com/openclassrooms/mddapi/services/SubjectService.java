package com.openclassrooms.mddapi.services;

import com.openclassrooms.mddapi.domain.Subject;
import com.openclassrooms.mddapi.domain.User;
import com.openclassrooms.mddapi.dto.SubjectDto;
import com.openclassrooms.mddapi.mapper.SubjectMapper;
import com.openclassrooms.mddapi.repository.SubjectRepository;
import com.openclassrooms.mddapi.repository.UserRepository;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class SubjectService {

    private final SubjectRepository subjectRepository;
    private final SubjectMapper subjectMapper;

    private final UserRepository userRepository;


    public SubjectService(SubjectRepository subjectRepository,
                          SubjectMapper subjectMapper,
                          UserRepository userRepository){
        this.subjectRepository = subjectRepository;
        this.subjectMapper = subjectMapper;
        this.userRepository = userRepository;
    }
    @Transactional(readOnly = true)
    public List<SubjectDto> getAll() {
        List<Subject> subjects = subjectRepository.findAll();
        return subjects.stream().map(subjectMapper::toDto).toList();
    }

    @Transactional(readOnly = true)
    public Subject getById(Integer subjectId) {
        return subjectRepository.findById(subjectId).orElse(null);
    }

    public Subject getByName(String subjectTitle) {
        return subjectRepository.findByTitle(subjectTitle).orElse(null);
    }

    @Transactional(readOnly = true)
    public List<SubjectDto> getUserSubjects(String userMail) {
        User currentUser = userRepository.findByEmail(userMail).orElse(null);

        List<Subject> subjects = subjectRepository.findByUserId(currentUser.getId()).orElse(null);
        return subjects.stream().map(subjectMapper::toDto).toList();
    }
}
