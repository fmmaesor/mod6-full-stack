package com.openclassrooms.mddapi.services;

import com.openclassrooms.mddapi.domain.Article;
import com.openclassrooms.mddapi.domain.Role;
import com.openclassrooms.mddapi.domain.Subject;
import com.openclassrooms.mddapi.domain.User;
import com.openclassrooms.mddapi.dto.UserDto;
import com.openclassrooms.mddapi.dto.UserRequest;
import com.openclassrooms.mddapi.mapper.SubjectMapper;
import com.openclassrooms.mddapi.mapper.UserMapper;
import com.openclassrooms.mddapi.repository.SubjectRepository;
import com.openclassrooms.mddapi.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserService {

  private final UserRepository userRepository;
  private final UserMapper userMapper;
  private final PasswordEncoder passwordEncoder;

  private final SubjectService subjectService;
  private final SubjectMapper subjectMapper;
  private final SubjectRepository subjectRepository;

  @PersistenceContext
  private final EntityManager entityManager;

  @Autowired
  private AuthenticationManager authenticationManager;


  public UserService(UserRepository userRepository,
                     UserMapper userMapper,
                     PasswordEncoder passwordEncoder,
                     SubjectService subjectService,
                     SubjectMapper subjectMapper,
                     SubjectRepository subjectRepository,
                     EntityManager entityManager) {
    this.userRepository = userRepository;
    this.userMapper = userMapper;
    this.passwordEncoder = passwordEncoder;
    this.subjectService = subjectService;
    this.subjectMapper = subjectMapper;
    this.subjectRepository = subjectRepository;
    this.entityManager = entityManager;
  }

  @Transactional(readOnly = true)
  public List<UserDto> getAll() {
      List<User> users = userRepository.findAll();
      return users.stream().map(userMapper::toDto).toList();
  }

  @Transactional(readOnly = true)
  public UserDto getById(Integer id) {
    return userRepository.findById(id)
      .map(userMapper::toDto)
      .orElse(null);
  }

  @Transactional(readOnly = true)
  public Optional<UserDto> getByName(String name) {
    return userRepository.findByName(name).map(userMapper::toDto);
  }

  @Transactional(readOnly = true)
  public Optional<UserDto> getByEmail(String email) {
    return userRepository.findByEmail(email).map(userMapper::toDto);
  }

  @Transactional(readOnly = true)
  public UserDto getCurrentUser() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    User currentUser = userRepository.findByName(authentication.getName()).orElse(null);
    if (currentUser != null) {
      return this.userMapper.toDto(currentUser);
    }
    return null;
  }

  public UserDto create(UserRequest userRequest) {
    validatePassword(userRequest.getPassword());

    UserDto user = new UserDto();
    user.setName(userRequest.getName());
    user.setEmail(userRequest.getEmail());
    user.setCreated_at(LocalDateTime.now());
    user.setUpdated_at(LocalDateTime.now());

    Role role = this.entityManager.find(Role.class, 2);

    if (role == null) {
      // Handle the case where the role doesn't exist
      throw new RuntimeException("Role not found with ID: " + user.getRoleId());
    }

    User userEntity = userMapper.toEntity(user);

    userEntity.setRole(role);
    userEntity.setPassword(this.passwordEncoder.encode(userRequest.getPassword()));
    userEntity.setSubjects(Collections.emptySet());

    User savedUser = userRepository.save(userEntity);
    return userMapper.toDto(savedUser);
  }

  public Optional<UserDto> update(String email, String newName, String newPassword) {
        return userRepository.findByEmail(email)
      .map(user -> {
        if (!"".equals(newName)) {
          user.setName(newName);
        }

        if (!"".equals(newPassword)) {
            validatePassword(newPassword);
          user.setPassword(this.passwordEncoder.encode(newPassword));

        }

        user.setUpdatedAt(LocalDateTime.now());

        return userMapper.toDto(userRepository.save(user));
      });
  }

  public Optional<UserDto> subscribeSubject(String userMail, Integer subjectId) {
    return userRepository.findByEmail(userMail)
            .map(user -> {
                Subject subject = subjectService.getById(subjectId);
                if (subject.getId() == null) {
                  subject = subjectRepository.save(subject);
                }
                if (!user.getSubjects().contains(subject)) {
                    user.getSubjects().add(subject);
                }
              return userMapper.toDto(userRepository.save(user));
            });
  }

  public Optional<UserDto> unSubscribeSubject(String userMail, Integer subjectId) {
    return userRepository.findByEmail(userMail)
            .map(user -> {
                Subject subject = subjectService.getById(subjectId);
                if (subject.getId() == null) {
                  subject = subjectRepository.save(subject);
                }
                user.getSubjects().remove(subject);
              return userMapper.toDto(userRepository.save(user));
            });
  }

  public boolean delete(Integer userId) {
    Optional<User> existingUser = userRepository.findById(userId);

    if (existingUser.isPresent()) {
      userRepository.delete(existingUser.get());
      return true;
    }
    return false; // User not found
  }

    private void validatePassword(String password) {
        // Define regular expressions for each required criteria
        boolean hasNumber = password.matches(".*\\d.*");
        boolean hasLowerCase = password.matches(".*[a-z].*");
        boolean hasUpperCase = password.matches(".*[A-Z].*");
        boolean hasSpecialChar = password.matches(".*[!@#$%^&*()_+\\-=\\[\\]{};':\"\\\\|,.<>\\/?].*");

        // Check if the password meets all criteria
        if (!(hasNumber && hasLowerCase && hasUpperCase && hasSpecialChar && password.length() >= 8)) {
            throw new IllegalArgumentException("Password must contain at least one number, one lowercase letter, one uppercase letter, one special character, and be at least 8 characters long.");
        }
    }


}
