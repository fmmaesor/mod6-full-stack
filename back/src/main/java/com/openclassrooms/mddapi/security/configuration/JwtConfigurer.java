package com.openclassrooms.mddapi.security.configuration;

import com.openclassrooms.mddapi.security.JwtTokenProvider;
import com.openclassrooms.mddapi.security.utils.filters.JwtFilter;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

public class JwtConfigurer extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {

  public static final String AUTHORIZATION_HEADER = "Authorization";

  private final JwtTokenProvider tokenProvider;

  public JwtConfigurer(JwtTokenProvider tokenProvider) {
    this.tokenProvider = tokenProvider;
  }

  /**
   * Configure.
   *
   * @param http the http
   */
  @Override
  public void configure(final HttpSecurity http) {
    final JwtFilter customFilter = new JwtFilter(tokenProvider);
    http.addFilterBefore(customFilter, UsernamePasswordAuthenticationFilter.class);
  }
}
