package com.openclassrooms.mddapi.security;


import com.openclassrooms.mddapi.domain.User;
import io.jsonwebtoken.*;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.stream.Collectors;

@Component
public class JwtTokenProvider {

  public static final String AUTHORITIES_KEY = "roles";

  private String jwtSecret;

  @Value("${expiration.time}")
  public long EXPIRATION_TIME;


  @Autowired
  public JwtTokenProvider(@Value("${jwtSecret}") String jwtSecret) {
    this.jwtSecret = jwtSecret;
  }

  public String generateJwtToken(Authentication authentication) {
    User customUser = (User) authentication.getPrincipal();

    var authorities = authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.joining(","));

    String createdAt;
    String updatedAt;

    try {
      DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");

      createdAt = customUser.getCreatedAt().format(formatter);
      updatedAt = customUser.getUpdatedAt().format(formatter);
    } catch (DateTimeException e) {
      e.printStackTrace();
      return null;
    }

    return Jwts.builder()
      .claim("Authorities", authorities)
      .claim("email", customUser.getEmail())
      .claim("createdAt", createdAt)
      .claim("updatedAt", updatedAt)
      .claim("role", customUser.getRole().getName()) // Assuming 'Role' has a 'getName' method
      .setSubject(authentication.getName())
      .setIssuedAt(new Date())
      .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
      .signWith(getSigningKey())
      .compact();

  }

  public boolean validateToken(String authToken) {
    try {
      Jwts.parserBuilder().setSigningKey(getSigningKey()).build().parseClaimsJws(authToken);
      return true;
    } catch (JwtException ex) {
      handleJwtException(ex);
    }
    return false;
  }

  public Authentication getAuthentication(final String token) {
    final Claims claims = Jwts.parserBuilder().setSigningKey(getSigningKey()).build().parseClaimsJws(token)
      .getBody();

    // Extract authorities if present
    Collection<? extends GrantedAuthority> authorities = Collections.emptyList();
    if (claims.containsKey(AUTHORITIES_KEY)) {
      authorities = Arrays.stream(claims.get(AUTHORITIES_KEY).toString().split(","))
        .map(SimpleGrantedAuthority::new)
        .collect(Collectors.toList());
    }
    // Extract additional fields
    String email = (String) claims.get("email");
    String createdAtString = (String) claims.get("createdAt");
    LocalDateTime createdAt = createdAtString != null ? LocalDateTime.parse(createdAtString) : null;
    String updatedAtString = (String) claims.get("updatedAt");
    LocalDateTime updatedAt = updatedAtString != null ? LocalDateTime.parse(updatedAtString) : null;

    final User principal = new User(claims.getSubject(), "", authorities);

    // Set additional fields using setters
    principal.setEmail(email);
    principal.setCreatedAt(createdAt);
    principal.setUpdatedAt(updatedAt);

    return new UsernamePasswordAuthenticationToken(principal, token, authorities);
  }

  private void handleJwtException(JwtException ex) {
    switch (ex.getClass().getSimpleName()) {
      case "SignatureException" -> System.err.println("Invalid JWT signature");
      case "MalformedJwtException" -> System.err.println("Invalid JWT token");
      case "ExpiredJwtException" -> System.err.println("JWT token is expired");
      case "UnsupportedJwtException" -> System.err.println("JWT token is unsupported");
      case "IllegalArgumentException" -> System.err.println("JWT claims string is empty");
      default -> System.err.println("Unexpected JWT exception: " + ex.getMessage());
    }
  }

  private SecretKey getSigningKey() {
    byte[] keyBytes = Decoders.BASE64.decode(jwtSecret);
    return Keys.hmacShaKeyFor(keyBytes);
  }
}
