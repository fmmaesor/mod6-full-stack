package com.openclassrooms.mddapi.web;

import com.openclassrooms.mddapi.dto.CommentDto;
import com.openclassrooms.mddapi.dto.CommentRequest;
import com.openclassrooms.mddapi.services.CommentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/comments")
public class CommentController {

    private final CommentService commentService;

    public CommentController(CommentService commentService){
        this.commentService = commentService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<List<CommentDto>> getACommentByArticleId(@PathVariable Integer id) {
        List<CommentDto> commentsByArticleId = commentService.getByArticleId(id);
        return commentsByArticleId != null
                ? ResponseEntity.ok(commentsByArticleId)
                : ResponseEntity.notFound().build();
    }

    @PostMapping("/create/{userEmail}")
    public ResponseEntity<CommentDto> createComment(@PathVariable String userEmail, @RequestBody CommentRequest commentRequest) {
        return new ResponseEntity<>(commentService.create(userEmail, commentRequest), HttpStatus.CREATED);
    }
}
