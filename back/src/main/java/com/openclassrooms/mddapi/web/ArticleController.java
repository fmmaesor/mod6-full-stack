package com.openclassrooms.mddapi.web;

import com.openclassrooms.mddapi.dto.ArticleDto;
import com.openclassrooms.mddapi.dto.ArticleRequest;
import com.openclassrooms.mddapi.services.ArticleService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/articles")
public class ArticleController {

    private final ArticleService articleService;

    public ArticleController(ArticleService articleService){
        this.articleService = articleService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<List<ArticleDto>> getArticlesBySubjectId(@PathVariable Integer id) {
        List<ArticleDto> articlesBySubjectId = articleService.getBySubjectId(id);
        return articlesBySubjectId != null
                ? ResponseEntity.ok(articlesBySubjectId)
                : ResponseEntity.notFound().build();
    }

    @PostMapping("/create/{userMail}")
    public ResponseEntity<ArticleDto> createArticle(@PathVariable String userMail, @RequestBody ArticleRequest articleRequest) {
        return new ResponseEntity<>(articleService.create(userMail, articleRequest), HttpStatus.CREATED);
    }

    @GetMapping("/feed/{userMail}")
    public ResponseEntity<List<ArticleDto>> getFeed(@PathVariable String userMail) {
        List<ArticleDto> feed = articleService.getFeed(userMail);
        return feed != null
                ? ResponseEntity.ok(feed)
                : ResponseEntity.notFound().build();
    }
}
