package com.openclassrooms.mddapi.web;

import com.openclassrooms.mddapi.domain.User;
import com.openclassrooms.mddapi.security.JwtTokenProvider;
import com.openclassrooms.mddapi.security.utils.JwtResponse;
import com.openclassrooms.mddapi.dto.LoginRequest;
import com.openclassrooms.mddapi.services.CustomUserDetailsService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/auth")
public class LoginController {


  private final AuthenticationManager authenticationManager;
  private final JwtTokenProvider jwtTokenProvider;
  private final CustomUserDetailsService customUserDetailsService;


  public LoginController(AuthenticationManager authenticationManager, JwtTokenProvider jwtTokenProvider, CustomUserDetailsService customUserDetailsService) {
    this.authenticationManager = authenticationManager;
    this.jwtTokenProvider = jwtTokenProvider;
    this.customUserDetailsService = customUserDetailsService;
  }

  @PostMapping("/login")
  public ResponseEntity<JwtResponse> login(@RequestBody final LoginRequest login) {
    String identifier = login.getIdentifier();
    try {
      UserDetails userDetails = customUserDetailsService.loadUserByUsername(identifier);

      String usernameOrEmail = userDetails.getUsername();

      final UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
              usernameOrEmail, login.getPassword());

    final Authentication authentication = this.authenticationManager.authenticate(authenticationToken);
    final SecurityContext securityContext = SecurityContextHolder.getContext();
    securityContext.setAuthentication(authentication);
    final String jwt =  this.jwtTokenProvider.generateJwtToken(authentication);
    final HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.add("Authorization", "Bearer " + jwt);
    return new ResponseEntity<>(new JwtResponse(jwt), httpHeaders, HttpStatus.OK);
    } catch (UsernameNotFoundException e) {
      // Handle invalid credentials (identifier not found)
      return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }
  }

}
