package com.openclassrooms.mddapi.web;

import com.openclassrooms.mddapi.dto.SubjectDto;
import com.openclassrooms.mddapi.services.SubjectService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/subjects")
public class SubjectController {

    private final SubjectService subjectService;

    public SubjectController(SubjectService subjectService){
        this.subjectService = subjectService;
    }

    @GetMapping
    public ResponseEntity<List<SubjectDto>> getAll() {
        List<SubjectDto> subjectDtoList = subjectService.getAll();
        return ResponseEntity.ok(subjectDtoList);
    }

    @GetMapping("/user/{userMail}")
    public ResponseEntity<List<SubjectDto>> getUserSubjects(@PathVariable String userMail) {
        List<SubjectDto> subjectDtoList = subjectService.getUserSubjects(userMail);
        return ResponseEntity.ok(subjectDtoList);
    }

}
