package com.openclassrooms.mddapi.web;

import com.openclassrooms.mddapi.dto.SubjectDto;
import com.openclassrooms.mddapi.dto.UserDto;
import com.openclassrooms.mddapi.dto.UserRequest;
import com.openclassrooms.mddapi.mapper.UserMapper;
import com.openclassrooms.mddapi.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UserController {

  private final UserService userService;
  private final UserMapper userMapper;

  @Autowired
  public UserController(UserService userService, UserMapper userMapper) {
    this.userService = userService;
    this.userMapper = userMapper;
  }

  @GetMapping
  public ResponseEntity<List<UserDto>> getAll() {
    List<UserDto> userDtoList = userService.getAll();
    return ResponseEntity.ok(userDtoList);
  }

  @GetMapping("/me")
  public ResponseEntity<UserDto> getCurrentUser() {
    UserDto userDto = userService.getCurrentUser();
    if (userDto != null) return ResponseEntity.ok(userDto);

    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
  }

  @GetMapping("/{id}")
  public ResponseEntity<UserDto> getById(@PathVariable Integer id) {
    UserDto userDto = userService.getById(id);
    return (userDto != null)
      ? new ResponseEntity<>(userDto, HttpStatus.OK)
      : new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  @GetMapping("/name/{name}")
  public ResponseEntity<UserDto> getByName(@PathVariable String name) {
    Optional<UserDto> userDtoOptional = userService.getByName(name);
    return userDtoOptional.map(userDto -> new ResponseEntity<>(userDto, HttpStatus.OK))
            .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @PostMapping("/create")
  public ResponseEntity<UserDto> create(@RequestBody UserRequest user) {
    UserDto createdUser = userService.create(user);
    return new ResponseEntity<>(createdUser, HttpStatus.CREATED);
  }

  @PutMapping("/update/{userMail}")
  public ResponseEntity<UserDto> update(@PathVariable String userMail, @RequestBody UserRequest userRequest) {
    Optional<UserDto> updatedUser = userService.update(userMail, userRequest.getName(), userRequest.getPassword());
    return updatedUser.map(userDto -> new ResponseEntity<>(userDto, HttpStatus.OK))
      .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @PutMapping("/subscribe/{userMail}")
  public ResponseEntity<UserDto> subscribe(@PathVariable String userMail, @RequestBody Integer subjectId){
    return new ResponseEntity<>(userService.subscribeSubject(userMail, subjectId).orElse(null), HttpStatus.OK);
  }

  @PutMapping("/unsubscribe/{userMail}")
  public ResponseEntity<UserDto> unsubscribe(@PathVariable String userMail, @RequestBody Integer subjectId){
    return new ResponseEntity<>(userService.unSubscribeSubject(userMail, subjectId).orElse(null), HttpStatus.OK);
  }

  @DeleteMapping("/delete/{userId}")
  public ResponseEntity<String> delete(@PathVariable Integer userId) {
    boolean deleted = userService.delete(userId);
    return (deleted)
      ? new ResponseEntity<>("User deleted successfully", HttpStatus.OK)
      : new ResponseEntity<>("User not found", HttpStatus.NOT_FOUND);
  }
}
