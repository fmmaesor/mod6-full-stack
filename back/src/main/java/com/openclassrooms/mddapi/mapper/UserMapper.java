package com.openclassrooms.mddapi.mapper;

import com.openclassrooms.mddapi.domain.Role;
import com.openclassrooms.mddapi.domain.Subject;
import com.openclassrooms.mddapi.domain.User;
import com.openclassrooms.mddapi.dto.UserDto;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;


@Component
public class UserMapper {


  public UserDto toDto(User user) {
    UserDto userDto = new UserDto();
    userDto.setId(user.getId());
    userDto.setName(user.getName());
    userDto.setEmail(user.getEmail());
    userDto.setCreated_at(user.getCreatedAt());
    userDto.setUpdated_at(user.getUpdatedAt());
    if (user.getRole() != null) {
      userDto.setRoleId( user.getRole().getId());
    }
    Set<Integer> subjectIds = new HashSet<>();
    for (Subject subject : user.getSubjects()) {
      subjectIds.add(subject.getId());
    }
    userDto.setSubjectIds(subjectIds);
    return userDto;
  }
  public User toEntity(UserDto userDto) {
    User user = new User();
    user.setId(userDto.getId());
    user.setName(userDto.getName());
    user.setEmail(userDto.getEmail());
    user.setCreatedAt(userDto.getCreated_at());
    user.setUpdatedAt(userDto.getUpdated_at());

    Role role = new Role();
    role.setId(userDto.getRoleId());  // Assuming Role has a setId method
    user.setRole(role);


    return user;
  }

}
