package com.openclassrooms.mddapi.mapper;

import com.openclassrooms.mddapi.domain.Subject;
import com.openclassrooms.mddapi.dto.SubjectDto;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
public class SubjectMapper {
    public SubjectDto toDto(Subject subject){
        SubjectDto dto = new SubjectDto();
        dto.setId(subject.getId());
        dto.setTitle(subject.getTitle());
        dto.setDescription(subject.getDescription());
        return dto;
    }

    public Subject toEntity(SubjectDto subjectDto){
        Subject entity = new Subject();
        entity.setTitle(subjectDto.getTitle());
        entity.setDescription(subjectDto.getDescription());
        return entity;
    }
}
