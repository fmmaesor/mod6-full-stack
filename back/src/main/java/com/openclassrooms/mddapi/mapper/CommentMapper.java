package com.openclassrooms.mddapi.mapper;


import com.openclassrooms.mddapi.domain.Article;
import com.openclassrooms.mddapi.domain.Comment;
import com.openclassrooms.mddapi.domain.User;
import com.openclassrooms.mddapi.dto.CommentDto;
import org.springframework.stereotype.Component;

    @Component
    public class CommentMapper {
        public CommentDto toDto(Comment comment){
            CommentDto dto = new CommentDto();
            dto.setDescription(comment.getDescription());
            dto.setCreatedAt(comment.getCreatedAt());
            dto.setArticleId(comment.getArticle().getId());
            dto.setUserId(comment.getUser().getId());
            return dto;
        }

        public Comment toEntity(CommentDto commentDto) {
            Comment comment = new Comment();
            comment.setDescription(commentDto.getDescription());
            comment.setCreatedAt(commentDto.getCreatedAt());
            User user = new User();
            user.setId(commentDto.getUserId());
            comment.setUser(user);
            Article article = new Article();
            article.setId(commentDto.getArticleId());
            comment.setArticle(article);
            return comment;
        }
}
