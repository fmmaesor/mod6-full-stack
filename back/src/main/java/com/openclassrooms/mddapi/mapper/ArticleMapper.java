package com.openclassrooms.mddapi.mapper;

import com.openclassrooms.mddapi.domain.Article;
import com.openclassrooms.mddapi.domain.Subject;
import com.openclassrooms.mddapi.domain.User;
import com.openclassrooms.mddapi.dto.ArticleDto;
import org.springframework.stereotype.Component;

@Component
public class ArticleMapper {
    public ArticleDto toDto(Article article){
        ArticleDto dto = new ArticleDto();
        dto.setId(article.getId());
        dto.setTitle(article.getTitle());
        dto.setDescription(article.getDescription());
        dto.setCreatedAt(article.getCreatedAt());
        dto.setUserId(article.getUser().getId());
        dto.setSubjectId(article.getSubject().getId());
        return dto;
    }

    public Article toEntity(ArticleDto articleDto) {
        Article article = new Article();
        article.setTitle(articleDto.getTitle());
        article.setDescription(articleDto.getDescription());
        article.setCreatedAt(articleDto.getCreatedAt());
        User user = new User();
        user.setId(articleDto.getUserId());
        article.setUser(user);
        Subject subject = new Subject();
        subject.setId(articleDto.getSubjectId());
        article.setSubject(subject);
        return article;
    }
}
