package com.openclassrooms.mddapi.repository;

import com.openclassrooms.mddapi.domain.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface CommentRepository extends JpaRepository<Comment, Integer> {
    Optional<Comment> findById (Integer id);

    @Query("SELECT e FROM Comment e WHERE e.article.id = :articleId")
    List<Comment> findByArticleId(@Param("articleId")  Integer articleId);
}
