package com.openclassrooms.mddapi.repository;

import com.openclassrooms.mddapi.domain.Article;
import org.springframework.data.jpa.repository.JpaRepository;

import com.openclassrooms.mddapi.domain.Subject;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface SubjectRepository extends JpaRepository<Subject, Integer> {
        Optional<Subject> findById (Integer id);

        @Query("SELECT e FROM Subject e WHERE e.title = :title")
        Optional<Subject> findByTitle(@Param("title")String title);

        @Query("SELECT u.subjects FROM User u WHERE u.id = :userId")
        Optional<List<Subject>> findByUserId(@Param("userId")Integer userId);
}
