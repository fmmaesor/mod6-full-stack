package com.openclassrooms.mddapi.repository;

import com.openclassrooms.mddapi.domain.Article;
import com.openclassrooms.mddapi.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ArticleRepository extends JpaRepository<Article, Integer> {
    Optional<Article> findById (Integer id);

    @Query("SELECT e FROM Article e WHERE e.title = :title")
    Optional<Article> findByTitle(@Param("title") String title);

    @Query("SELECT e FROM Article e WHERE e.subject.id = :subjectId")
    List<Article> findBySubjectId(@Param("subjectId")  Integer subjectId);
}
