package com.openclassrooms.mddapi.repository;

import com.openclassrooms.mddapi.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {

  Optional<User> findByName(String name);

  Optional<User> findByEmail(String email);
}
